FROM ruby:2.7.0

RUN apt-get update && apt-get install -y \
  build-essential

COPY . /app
WORKDIR /app

RUN gem install bundler
RUN bundle install --jobs 20 --retry 5
