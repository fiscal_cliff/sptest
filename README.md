# Sptest

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/sptest`. To experiment with that code, run `bin/console` for an interactive prompt.

Write a ruby script that:
    a. Receives a log as argument (webserver.log is provided) e.g.: ./parser.rb webserver.log
    b. Returns the following:
        > list of webpages with most page views ordered from most pages views to less page views e.g.:
            /home 90 visits /index 80 visits etc... > list of webpages with most unique page views also ordered
            e.g.:
            /about/2 8 unique views /index 5 unique views etc...

Finally, have some fun – Feel free to make changes or design something if you think it meets the criteria above, but would produce better outcomes and of course, the sooner you return the test, the quicker we can move the process.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'sptest'
```

And then execute:

```bash
$ bundle install
```

Or install it yourself as:

```bash
$ gem install sptest
```

## Usage

The executable is in exe directory. You may invoke it in following ways:

    $ bundle exec sptest

    Commands:
      sptest help [COMMAND]     # Describe available commands or one specific command
      sptest sptest parse PATH  # Represents statistics from log files

      sptest sptest parse PATH

    Represents statistics from log file in PATH to the file -> test_2020-09-16 01:29:41 +0200.txt

There are two representers available

```bash
$ bundle exec sptest parse ./ruby_app/webserver.log --format json
```
or
```bash
$ bundle exec sptest parse ./ruby_app/webserver.log --format text
```

## Dockerfile

You may use this Docker file for improved isolation from your host system:

```bash
$ docker build --build-arg RAILS_ENV=test -t sptest .
```
```bash
$ docker run -it --rm -eRUBY_ENV=test sptest bundle exec rspec
```
```bash
$ docker run -it --rm -eRUBY_ENV=test sptest bundle exec rubocop
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/fiscal_cliff/sptest. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://gitlab.com/fiscal_cliff/sptest/blob/master/CODE_OF_CONDUCT.md).


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Sptest project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/fiscal_cliff/sptest/blob/master/CODE_OF_CONDUCT.md).
