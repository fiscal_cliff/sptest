require "thor"
require "sptest/presenter"
require "sptest/presenter/json"
require "sptest/presenter/text"

module Sptest
  class CLI < Thor
    desc("sptest parse PATH", "Represents statistics from log files")
    option :format, type: :string, default: "text", enum: %w[text json]

    def parse(path)
      service = Sptest::Parser.new(path: path, format: options[:format])
      service.call
      presenter = Sptest::Presenter.build(service)
      presenter.render_to_file("test_#{Time.now}")
    rescue Sptest::Error => e
      puts "An Error has occured: #{e.message}"
    end
  end
end
