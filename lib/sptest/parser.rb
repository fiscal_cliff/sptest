
module Sptest
  class Parser
    LOG_FORMAT = /^(?<path>[^\ ]+)\ +(?<ip>\d{0,3}\.\d{0,3}.\d{0,3}\.\d{0,3})$/
    WRONG_MESSAGE = "Wrong format value, expected one of: text, json".freeze

    attr_reader :stats, :path, :format, :errors

    def initialize(path:, format:)
      @path = path
      @format = format
      @errors = []
      @stats = {}
    end

    def call
      raise(Sptest::Error, errors.join("; ")) unless valid_args?
      for_each_line(&method(:process_line))
    end

    private

    attr_writer :stats

    def valid_args?
      errors << "File #{path} not exists for some reason!" unless File.exist?(path)
      errors << WRONG_MESSAGE unless %w[text json].include?(format)
      errors.empty?
    end

    def for_each_line(&block)
      File.open(path, "r") do |f|
        f.each_line(&block)
      end
    end

    def process_line(line)
      matched_info = line.match(LOG_FORMAT)
      path, ip = matched_info.values_at(:path, :ip)
      collect_stats(path: path, ip: ip)
    rescue StandardError => e
      message = "An error has occured: #{e.class}: #{e.message} while processing line: #{line}"
      raise(Sptest::Error, message)
    end

    def collect_stats(path:, ip:)
      stats[path] ||= {}
      stats[path][ip] ||= 0
      stats[path][ip] += 1 # that's all the hits. not unique enough
    end
  end
end
