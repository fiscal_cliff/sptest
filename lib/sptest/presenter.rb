
module Sptest
  module Presenter
    def self.build(service)
      return Sptest::Presenter::Json.new(service) if service.format == "json"
      return Sptest::Presenter::Text.new(service) if service.format == "text"

      raise Sptest::Error, "Unknown format: #{service.format}"
    end
  end
end
