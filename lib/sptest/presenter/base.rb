
module Sptest
  module Presenter
    class Base
      attr_reader :service

      def initialize(service)
        @service = service
      end

      def render
        build_view_model
      end

      def render_to_file(name_pattern)
        old_stdout = $stdout
        fd = IO.sysopen("#{name_pattern}.#{extension}", "w")
        IO.open(fd, "w") do |f|
          $stdout = f
          f.write(render)
        end
        $stdout = old_stdout
        puts("File #{name_pattern}.#{extension} has been saved...")
      end
    end
  end
end
