require "sptest/presenter/base"
require "json"

module Sptest
  module Presenter
    class Json < Base
      def render
        JSON.dump(super)
      end

      def build_view_model
        service.stats.sort_by { |_path, ips| -ips.size }
              .map { |path, ips| { path: path, hosts: ips.size, hits: ips.map(&:last).flatten.sum } }
      end

      private

      def extension
        "json"
      end
    end
  end
end
