require "sptest/presenter/base"

module Sptest
  module Presenter
    class Text < Base
      HEADERS = %w[paths hosts hits].freeze

      def render
        Thor::Shell::Basic.new.print_table(super)

        nil
      end

      def build_view_model
        [HEADERS] +
          service.stats.sort_by { |_path, ips| -ips.size }
                .map { |path, ips| [path, ips.size, ips.map(&:last).flatten.sum] }
      end

      private

      def extension
        "txt"
      end
    end
  end
end
