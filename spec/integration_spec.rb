require "open3"

RSpec.describe "Integration test of" do
  let(:fixture) { "spec/fixtures/2p10u.log" }
  let(:command) { "exe/sptest" }
  let!(:iop) { Open3.popen2(command) }

  context "application without parameters" do
    let(:command) { "exe/sptest" }

    it "shows usage message" do
      expect(iop[1].read).to match(/Represents statistics from/)
    end
  end

  context "right command invoked" do
    context "with default parameters" do
      let(:command) { "exe/sptest parse #{fixture}" }

      it "parses 2p10u fixture correctly" do
        expect(iop[1].read).to match(/File test_.*\.txt has been saved\./)
      end
    end

    context "with text format" do
      let(:command) { "exe/sptest parse #{fixture} --format text" }

      it "parses 2p10u fixture correctly" do
        expect(iop[1].read).to match(/File test_.*\.txt has been saved\./)
      end
    end

    context "with json format" do
      let(:command) { "exe/sptest parse #{fixture} --format json" }

      it "parses 2p10u fixture correctly" do
        expect(iop[1].read).to match(/File test_.*\.json has been saved\./)
      end
    end
  end
end
