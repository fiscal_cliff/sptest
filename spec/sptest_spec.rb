RSpec.describe Sptest do
  let(:path) { "spec/fixtures/synthetic.log" }

  it "has a version number" do
    expect(Sptest::VERSION).not_to be nil
  end

  context "service validations" do
    let(:format) { "text" }
    let(:service) { Sptest::Parser.new(path: path, format: format) }

    context "with wrong parameters" do
      let(:format) { "wrong" }

      it "does not parse synthetic" do
        expect { service.call }.to raise_error(Sptest::Error)
        expect(service.stats).to eq({})
      end
    end

    context "with wrong path" do
      let(:path) { "spec/fixtures/synthetic.log" }

      it "does not parse synthetic" do
        expect(service.stats).to eq({})
      end
    end
  end

  context "right service invoked" do
    let(:format) { "text" }
    let(:service) { Sptest::Parser.new(path: path, format: format) }
    before { service.call }

    let(:response) do
      {
        "/path1" => {
          "1.111.1.45" => 1,
          "1.111.1.47" => 1,
          "1.111.1.49" => 1,
          "1.222.1.44" => 3
        },
        "/path2" => {
          "1.111.1.46" => 1,
          "1.111.1.48" => 1,
          "1.111.1.50" => 1,
          "1.222.1.44" => 3
        },
        "/path3" => {
          "1.11.3.1" => 1
        },
        "/path4" => {
          "1.11.3.1" => 1
        }
      }
    end

    context "with text format" do
      let(:format) { "text" }

      let(:expectation) do
        "paths   hosts  hits\n/path1      4     6\n/path2      4" \
          "     6\n/path3      1     1\n/path4      1     1\n"
      end

      it "parses synthetic fixture correctly" do
        expect(service.stats).to eq(response)
      end

      it "renders table" do
        expect(Sptest::Presenter::Text.new(service).render_to_file("test")).to eq(nil)
        expect(File.read("test.txt")).to eq(expectation)
      end
    end

    context "with json format" do
      let(:format) { "json" }

      let(:expectation) do
        '[{"path":"/path1","hosts":4,"hits":6},{"path":"/path2","hosts":4,"hits":6},' \
          '{"path":"/path3","hosts":1,"hits":1},{"path":"/path4","hosts":1,"hits":1}]'
      end

      it "parses synthetic fixture correctly" do
        expect(service.stats).to eq(response)
      end

      it "renders table" do
        expect(Sptest::Presenter::Json.new(service).render_to_file("test")).to eq(nil)
        expect(File.read("test.json")).to eq(expectation)
      end
    end
  end
end
