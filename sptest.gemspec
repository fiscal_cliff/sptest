require_relative "lib/sptest/version"

Gem::Specification.new do |spec|
  spec.name          = "sptest"
  spec.version       = Sptest::VERSION
  spec.authors       = ["el"]
  spec.email         = ["evgenii.lukovskii@yahoo.com"]

  spec.summary       = %q{Test assignment}
  spec.description   = %q{Test assignment parses log files and aggregates unique views by path}
  spec.homepage      = "https://gitlab.com/fiscal_cliff/sptest"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  # spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/fiscal_cliff/sptest"
  spec.metadata["changelog_uri"] = "https://gitlab.com/fiscal_cliff/sptest"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path("..", __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
end
